#from django.http import HttpResponse
from django.shortcuts import render

def homepage(request):
    #return HttpResponse("Hello, world. You're at the polls index.")
    return render(request, 'home.html')

def about(request):
   #return HttpResponse("This is the about page.")
   return render(request, 'about.html')


def users_list(request):
    return render(request, 'users-list.html')

def add_user(request):
    return render(request, 'add-user.html')

def edit_user(request):
    return render(request, 'edit-user.html')