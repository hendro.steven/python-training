from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'content', 'author']
        error_messages = {
            'title': {
                'required': "The title field is required."
            },
            'content': {
                'required': "The content field is required."
            },
            'author': {
                'required': "The author field is required."
            }
        }