from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from posts.models import Post
from .serializer import PostSerializer

# Create your views here.

@api_view(['GET'])
def welcome(request):
    return Response({'message':'Welcome to the Posts API'})


@api_view(['GET'])
def get_all_posts(request):
    posts = Post.objects.all()
    serilaizer = PostSerializer(posts, many=True)
    return Response(serilaizer.data)

@api_view(['GET'])
def post_detail(request, pk):
    post = Post.objects.get(id=pk)
    serilaizer = PostSerializer(post, many=False)
    return Response({'messages': 'Get data successfull', 'payload': serilaizer.data}, status=200)

@api_view(['POST'])
def post_create(request):
    serializer = PostSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    else:
        return Response(serializer.errors, status=400)

@api_view(['DELETE'])
def post_delete(request, pk):
    post = Post.objects.get(id=pk,)
    post.delete()
    return Response({'message':'Post deleted successfully'})
 
@api_view(['PUT'])   
def post_update(request, pk):
    post = Post.objects.get(id=pk)
    serializer = PostSerializer(instance=post, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=200)
    else:
        return Response(serializer.errors, status=400)