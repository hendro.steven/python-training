from django.urls import path
from . import views

urlpatterns = [
    path('',views.welcome),
    path('list/',views.get_all_posts, name='get_all_posts'),
    path('detail/<int:pk>/',views.post_detail, name='post_detail'),
    path('create/',views.post_create, name='post_create'),
    path('delete/<int:pk>/',views.post_delete, name='post_delete'),
    path('update/<int:pk>/',views.post_update, name='post_update'),
]