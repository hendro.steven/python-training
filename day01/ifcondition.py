"""
if condition:
    statement
"""

def demoIf():
   age = input('Enter your age: ')
   if(int(age) >= 18):
       print('You are eligible to vote')     
       print('Your are an adult')
   else:
       print('You are not eligible to vote')
   print('Thank you for using our application')
       
def demoElif():
    age = input('Enter your age: ')
    if(int(age) >= 18):
        print('You are eligible to vote')     
        print('Your are an adult')
        if(int(age) >= 60):
            print('You are a senior citizen')
            if(int(age) >= 80):
                print('You are a super senior citizen')
                if(int(age) >= 100):
                    print('You are a legend')
    elif(int(age) >= 13):
        print('You are a teenager')
    else:
        print('You are not eligible to vote')
    print('Thank you for using our application')

demoElif()