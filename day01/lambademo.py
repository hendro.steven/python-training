"""
lambda parameter_list: expression
"""

x = lambda a : a + 10
z = lambda a, b : a + b + 100


print(x(5))
print(z(5, 10))

