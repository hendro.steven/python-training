def main():
    a = 5
    b = 5.3
    
    print(type(a))
    print(type(b))
    
def stringData():
    name = "hendro steven"
    # print(type(name))
    # print(name[0:3])
    # print(name[10:])
    # print(len(name))
    # print(name.capitalize())
    # print(name.upper())
    # print(name.lower())
    i = 10
    x = len(name)
    print(name[i:x])
    
def listData():
    data = [1,2,3,4,5,6,7,8,9,10]
    print(data)
    print(type(data))
    
    data2 = [1,2, "hendro", "True", 2.5]
    print(data2)
    
    a = True
    b = False
    c = "True"
    print(type(a))
    print(type(b))
    print(type(c))
    
def tupleData():
    data = (1,2,3,4,5,6,7,8,9,10)
    print(data)
    print(type(data))
    
def dictionaryData():
    data = {
        "name" : "hendro steven",
        "age" : 25,
        "hobies" : ["eat", "sleep", "code"]
    }
    
    print(data)
    print(type(data))
    print(data["name"])
    print(data["age"])
    print(data["hoby"])
    
def setData():
    data = {1,2,2,3,4,5,6,7,8,9,10}
    print(data)
    print(type(data))
    
def castingDemo():
    a = "10"
    b = 10
    c = 10.5
    d = True
    
    print(type(a))
    print(type(b))
    print(type(c))
    print(type(d))
    
    print(int(a))
    print(float(b))
    print(int(c))
    print(bool(d))
    
def aritmaticsOperatorsDemo():
    a = 10
    b = 20
    
    print(a+b)
    print(a-b)
    print(a*b)
    print(a/b)
    print(a%b)
    print(a**b)

def logicalOperatorsDemo():
    a = True
    b = False
    
    print(a and b)
    print(a or b)
    print(not a)
    print(not b)

def comparisonOperatorsDemo():
    a = 10
    b = 20
    
    print(a == b)
    print(a != b)
    print(a > b)
    print(a < b)
    print(a >= b)
    print(a <= b)

def assigmentOperatorDemo():
    a = 10
    b = 20
    
    a += b
    print(a)
    
    a -= b
    print(a)
    
    a *= b
    print(a)
    
    a /= b
    print(a)
    
    a %= b
    print(a)
    
    a **= b
    print(a)
    
    a //= b
    print(a)
    
def bitwiseOperatorDemo():
    a = 1 
    b = 2 
    
    print(a & b)
   
def logicalOperatorsDemo():
    a = True
    b = False
    
    print(a and b)
    print(a or b)
    print(not a)
    print(not b)
    
def membershipOperatorsDemo():
    data = [1,2,3,4,5,6,7,8,9,10]
    print(1 in data)
    print(1 not in data)
    
def identityOperatorsDemo():
    a = 10
    b = 10
    print(id(a))
    print(id(b))
    print(a is b)
    print(a is not b)
    
    b = 20
    print(id(a))
    print(id(b))
    print(a is b)
    print(a is not b)
    
def constantDemo():
    PI = 3.14
    print(PI)
    
    PI = 3.15
    print(PI)
    
    pi = 3.16
    print(pi)
   
constantDemo()
