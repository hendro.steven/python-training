def printHello(name):
    print(f"Hello {name}")
    print("Welcome to Python")
    
def greeting(name):
    return f"Hello {name}"

def add(num1, num2):
    return num1 + num2

def checkEvenOdd(num):
    if num % 2 == 0:
        return "Even"
    else:
        return "Odd"
    
def functionReference(name):
    print(f"Hello {name}")
    name = "Steven"
    print(f"Hello {name}")
    
def increment(num):
    num += 1
    print(num)
    
def greet(name, counter):
    counter += 1
    print(id(counter))
    return f"Hi, {name}"

def main():
    counter = 0
    print(greet("Hendro", counter))
    print(id(counter))
    print(f"Counter: {counter}")
    print(greet("Steven", counter))
    print(f"Counter: {counter}")
    
main()
    
# printHello("Hendro Steven")
# print(greeting("Hendro Steven"))

# print(add(10, 20))

# result = checkEvenOdd(10)
# print(result)
