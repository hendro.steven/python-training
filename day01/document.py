def blockCommentDemo():
    # this is a comment
    print('Hello World')
    print('Hello Python') # this is a comment
    
def docStringDemo():
    """
    This is a doc string
    This is a doc string
    """
    
    '''
    This is a doc string
    This is a doc string
    This is a doc string
    This is a doc string
    This is a doc string
    '''
    
    print('Hello World')
    print('Hello Python')
    
docStringDemo()