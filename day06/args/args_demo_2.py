def my_fun(x, *args):
    print(x)
    for arg in args:
        print(arg)

my_fun('Hello', 'Welcome', 'to', 'Python', 'Programming', 'Language', 12, 14, 23)