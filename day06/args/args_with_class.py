class car():
    def __init__(self, *args):
        # if args[0] is not None:
        #     self.speed = args[0]
        # if args[1] is not None:
        #     self.color = args[1]
        self.speed = args[0]
        self.color = args[1]


audi = car(200, 'red')
bmw = car(250, 'blue')
mb = car(300, 'black')
other = car(100)

print(audi.speed)
print(bmw.color)