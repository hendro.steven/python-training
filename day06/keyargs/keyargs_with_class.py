class car():
    def __init__(self, **kwargs):
        self.model = kwargs['model']
        self.color = kwargs['color']
        self.speed = kwargs['speed']
        # print(len(kwargs))


audi = car(model='A6', color='black', speed=200)
bmw = car(model='X5', color='white', speed=250)
mb = car(model='S600', color='silver', speed=300)

print(audi.model)
print(bmw.color)
print(mb.speed)

string = '2000'
