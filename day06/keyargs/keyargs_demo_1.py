def my_fun(**kwargs):
    for key, value in kwargs.items():
        print(f"{key} = {value}")

my_fun(name="John", age=30, city="New York", country="USA", phone="1234567890")