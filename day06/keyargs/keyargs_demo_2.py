def my_fun(other, **kwargs):
    print(f"other = {other}")
    for key, value in kwargs.items():
        print(f"{key} = {value}")

my_fun("Welcome", name="John", age=30, city="New York", country="USA", phone="1234567890")