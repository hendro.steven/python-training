def fun(a):
    if a < 4:
        b = a/(a-3)

    print('Value of b = ', b)

try:
    fun(5)
except ZeroDivisionError:
    print('Zero Division Error Occurred and Handled')
except NameError:
    print('Name Error Occurred and Handled')
