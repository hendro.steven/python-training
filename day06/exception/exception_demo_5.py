def fun1(a, b):
    try:
        # Open Database connection or network connection
        # Perform database operations
        # logic
    except:
        # Rollback if any error --> there also error in rollback
        # print error message
    finally:
        # Close database connection or network connection
        # print message to indicate the end



def fun2(a, b):
    pass


def fun(a, b):
    try:
        c = ((a + b) / (a - b))
    except ZeroDivisionError:
        print("a/b result in 0")
    else:
        print(c)
    finally:
        print('This is the end of the function', c)

    print('Hi there')
    print('This is the end of the function')


fun(3.0, 2.0)
