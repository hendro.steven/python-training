a = [1, 2, 3]
try:
    print("Second element = %d" % (a[1]))
    print("Fourth element = %d" % (a[3]))
#     complex operation
except:
    print("An error occurred")