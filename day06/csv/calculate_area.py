import csv

total_area = 0

with open('country.csv', 'r') as file:
    csv_reader = csv.reader(file)

    # Skip the header
    next(csv_reader)

    for row in csv_reader:
        total_area += float(row[1])

print('Total Area = ',total_area)