import csv

with open('country.csv', 'r') as file:
    csv_reader = csv.reader(file)

    # Skip the header
    next(csv_reader)

    # Print the rows
    for row in csv_reader:
        print(row)