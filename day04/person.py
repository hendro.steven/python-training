class Person:
    # data = 10 # class variable
    # height = 0
    
    def __init__(self, age, height, name):  # constructor
        self.height = height
        self._name = name
        self.__age = age # private member
        
    def setAge(self, age):
        if age > 0:
            self.__age = age
            
    def getAge(self):
        return self.__age
    
    def setName(self, name):
        self._name = name
        
    def getName(self):
        return self._name
    

class Student(Person):
    
    def __init__(self, age, height, name):
        super().__init__(age, height, name)
    
    def __str__(self):
        return "Student: " + self._name + " " + str(self.height) 


    
p1 = Student(20, 180, "John")
p1.data = 20
print(p1.data)

p2 = Student(20, 180, "Mark")
p2.data = 30
print(p2.data)

print(Person.data)
