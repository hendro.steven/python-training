class School:
    def funcSchool(self):
        print("This function is in school.")
        
class Student1(School):
    def funcStudent1(self):
        print("This function is in student 1.")
        

class Student2(School):
    def funcStudent2(self):
        print("This function is in student 2.")
        
class Student3(Student1, School):
    def funcStudent3(self):
        print("This function is in student 3.")
        

s3 = Student3()
s3.funcSchool()
s3.funcStudent1()
s3.funcStudent3()
