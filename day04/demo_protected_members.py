class Base:
    def __init__(self):
        self.a = 10
        self._b = 20 # protected member
        self.__c = 30 # private member
    
    def updateC(self, c):
        # logic to update __c
        if c > 0:
            self.__c = c
            
    def getC(self):
        return self.__c
        
class Derived(Base):
    def __init__(self):
        Base.__init__(self)
        print("Calling protected member of base class: ", self._b)
        
        self._b = 30
        print("Calling protected member of base class: ", self._b)
        
        print("Calling private member of base class: ", self.__c) # will not work because its private
        
obj1 = Base()
#obj1.__c = 40 # will not change variable because its private
obj1.updateC(40)
print(obj1.getC())

obj1.updateC(-40)
print(obj1.getC())
        