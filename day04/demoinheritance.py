class Person:
    
    def __init__(self, name, age):
        print("Person constructor")
        self.name = name
        self.age = age

    def display(self):
        print("Name:", self.name)
        print("Age:", self.age)
        
class Employee(Person):
    
    def __init__(self, name, age, idnumber):
        super().__init__(name, age)
        self.idnumber = idnumber
        
    def printData(self):
        print("Name:", self.name)
        print("Age:", self.age)
        print("ID:", self.idnumber)
    
    
prs = Person("John", 30)
prs.display()
#prs.printData() # AttributeError: 'Person' object has no attribute 'printData'

emp = Employee("Jack", 35, "234242424")
emp.display()
emp.printData()
              
        
