class Base1:
    def __init__(self):
        self.str1 = "Base1"
        print("Base1 constructor")
    
    def printStr1(self):
        print(self.str1)

class Base2:
    def __init__(self):
        self.str2 = "Base2"        
        print("Base2 constructor")
        
    def printStr2(self):
        print(self.str2)
        
class Derived(Base1, Base2):
    def __init__(self):
        Base1.__init__(self)
        Base2.__init__(self)
        print("Derived constructor")
        
    def printStrs(self):
        print(self.str1, self.str2)
        
d = Derived()
d.printStrs()
d.printStr1()
d.printStr2()