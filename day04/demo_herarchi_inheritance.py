class Parent:
    def funcParent(self):
        print("This function is in parent class.")  
        
class Child1(Parent):
    def func1(self):
        print("This function is in child 1.")
        
class Child2(Parent):
    def func2(self):
        print("This function is in child 2")

class Child3(Child1, Child2):
    def func3(self):
        print("This function is in child 3.")
        
c1 = Child1()
c2 = Child2()
c3 = Child3()

c1.funcParent()
c2.funcParent()
c3.funcParent() 

c1.func1()
c2.func2()
c3.func3()