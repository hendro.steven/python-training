from machine import Pin, PWM
from time import sleep


print("Welcome to ESP32")

pwm = PWM(Pin(4), freq=50, duty=0)

def Servo(angle):
    pwm.duty(int(((angle)/180 *2 + 0.5) / 20 * 1023))
    
while True:
    Servo(180)
    sleep(2)
    Servo(90)
    sleep(2)
    Servo(0)
    sleep(2)