from machine import Pin, PWM
from time import sleep


print("Welcome to ESP32")

pwm = PWM(Pin(4), freq=50, duty=0)

def Servo(angle):
    pwm.duty(int(((angle)/180 *2 + 0.5) / 20 * 1023))

Servo(90)
    
def turnRight():
    posisi = 90;
    while posisi>=30:
        posisi = posisi - 1
        Servo(posisi)
        sleep(0.03)
    Servo(90)
    
def turnLeft():
    posisi = 90;
    while posisi<=150:
        posisi = posisi + 1
        Servo(posisi)
        sleep(0.03)
    Servo(90)
    
