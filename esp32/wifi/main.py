import machine
led = machine.Pin(2,machine.Pin.OUT)
led.off()

import network
import wifi_credentials
import time

wifi = network.WLAN(network.STA_IF)

#disconnect first
if wifi.isconnected():
    wifi.disconnect()
    wifi.active(False)
    time.sleep(1)

#create new connection
if not wifi.isconnected():
    print('connecting to network...')
    wifi.active(True)
    wifi.connect(wifi_credentials.ssid, wifi_credentials.password)
    while not wifi.isconnected():
        pass
print('Connected!')
print('Network config:', wifi.ifconfig())