from machine import Pin
from time import sleep

#motion = False

def handle_interrupt(pin):
    global motion
    motion=True
    global interrupt_pin
    interrupt_pin=pin
    
led =Pin(2,Pin.OUT)
pir=Pin(14,Pin.IN)
#pir.irq(trigger=Pin.IRQ_RISING,handler=handle_interrupt)
led.off()

while True:
    if pir.value():
        print('Motion detected!')
        led.on()
        sleep(2)
        led.off()
        print('Motion Stopped!')
        #motion = False