from machine import Pin, ADC
from time import sleep

#https://www.robotique.tech/robotics/fire-detection-system-controlled-by-esp32/

flame = ADC(Pin(4))
flame.atten(ADC.ATTN_11DB)
led = Pin(14, Pin.OUT)

  
def turnOn():
    led.on()
    
def turnOff():
    led.off()

while True:
    flame_value = flame.read()
    sleep(0.1)
    if(flame_value<4095):
        turnOn()
    else:
        turnOff()
