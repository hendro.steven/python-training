#https://randomnerdtutorials.com/micropython-whatsapp-esp32-esp826/
from machine import Pin, ADC
from time import sleep
import urequests as requests
import network
import wifi_credentials

import esp 
esp.osdebug(None)
import gc
gc.collect()

#Your phone number in international format
phone_number = '6281328339692'
#Your callmebot API key
api_key = '6618268'

flame = ADC(Pin(34))  #https://forum.micropython.org/viewtopic.php?t=9415
flame.atten(ADC.ATTN_11DB)
led = Pin(14, Pin.OUT)
buzzer = Pin(23, Pin.OUT)
wifi = network.WLAN(network.STA_IF)

def disconect_wifi():
    if wifi.isconnected():
        wifi.disconnect()
        wifi.active(False)
        sleep(1)
    

def connect_wifi():
    #Your network credentials
    ssid = 'AnyaMili_4G'
    password = 'Ria050715'
    
    #wifi = network.WLAN(network.STA_IF)

    #disconnect first
    disconect_wifi()

    #create new connection
    if not wifi.isconnected():
        print('connecting to network...')
        wifi.active(True)
        wifi.connect(wifi_credentials.ssid, wifi_credentials.password)
        while not wifi.isconnected():
            pass
    print('Connected!')
    print('Network config:', wifi.ifconfig())
        
# connect_wifi()

  
def send_message(phone_number, api_key, message):
  #set your host URL
  url = 'https://api.callmebot.com/whatsapp.php?phone='+phone_number+'&text='+message+'&apikey='+api_key  #https://www.callmebot.com/blog/free-api-whatsapp-messages/
  #make the request
  response = requests.get(url)
  #check if it was successful
  if response.status_code == 200:
    print('Success!')
  else:
    print('Error')
    print(response.text)

  
def turnOn():
    led.on()
    
def turnOff():
    led.off()
    

while True:
    flame_value = flame.read()
    sleep(0.1)
    if(flame_value<4095):
        connect_wifi()
        turnOn()
        buzzer.value(1)
        message = 'WARNING!%21%20there%20is%20a%20fire%20in%20your%20house%21' #YOUR MESSAGE HERE (URL ENCODED)https://www.urlencoder.io/ 
        send_message(phone_number, api_key, message)
        disconect_wifi()
    else:
        turnOff()
        buzzer.value(0)
    gc.collect()
