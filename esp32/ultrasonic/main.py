from machine import Pin
from machine import Timer
from time import sleep
from hcsr04 import HCSR04

print("Hello, ESP32!")

led = Pin(14, Pin.OUT)
ultrasonic = HCSR04(trigger_pin=2, echo_pin=4, echo_timeout_us=1000000)

  
def turnOn():
    led.on()
    
def turnOff():
    led.off()

turnOff()

def ultrasonic_callback(time):
    # print('Distance: {} cm'.format(ultrasonic.distance_cm()))
    distance = ultrasonic.distance_cm()
    print('Distance: {} cm'.format(distance))
    if distance <= 10:
        turnOn()
        print('Alert!! it is to close!')
    else:
        turnOff()

timer0 = Timer(0)
timer0.init(period=500, mode=Timer.PERIODIC, callback=ultrasonic_callback)