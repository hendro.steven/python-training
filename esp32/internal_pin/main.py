from machine import Pin
from time import sleep

print("Hello, ESP32!")

led = Pin(2, Pin.OUT)

  
def turnOn():
    led.on()
    
def turnOff():
    led.off()

while True:
    turnOn()
    sleep(0.5)in
    turnOff()
    sleep(0.5)
