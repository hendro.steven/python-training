from dcmotor import DCMotor
from machine import Pin, PWM, Timer
from time import sleep
from hcsr04 import HCSR04
import random

frequency = 15000

pin1 = Pin(12, Pin.OUT)
pin2 = Pin(14, Pin.OUT)
pin3 = Pin(26, Pin.OUT)
pin4 = Pin(27, Pin.OUT)
enable = PWM(Pin(13), frequency)
distance = 100

dc_motor1 = DCMotor(pin1, pin2, enable)
dc_motor2 = DCMotor(pin3, pin4, enable)
ultrasonic = HCSR04(trigger_pin=18, echo_pin=19, echo_timeout_us=1000000)
#Set min duty cycle (350) and max duty cycle (1023)
#dc_motor = DCMotor(pin1, pin2, enable, 350, 1023)

def forward(speed=100):
    print('Forward')
    dc_motor1.forward(speed)
    dc_motor2.forward(speed)
    
def backward(speed=100):
    print('Backward')
    dc_motor1.backwards(speed)
    dc_motor2.backwards(speed)
    
def turn_left():
    print('Turn Left')
    dc_motor1.stop()
    dc_motor2.forward(20)
    
def turn_right():
    dc_motor1.forward(20)
    dc_motor2.stop()
    
def stop():
    print('Stop')
    dc_motor1.stop()
    dc_motor2.stop()
    
def timer0_callback(timer):
  distance = ultrasonic.distance_cm()
  print('Distance: ', distance, ' cm')
  if distance <= 15:
      stop()
      backward()
      sleep(0.5)
      if random.randint(0, 1) == 1:
          turn_left()
      else:
          turn_right()
      sleep(0.5)
  else:
      forward()
    
timer0 = Timer(0)
timer0.init(period=500, mode=Timer.PERIODIC, callback=timer0_callback)
    
