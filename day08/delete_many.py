from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client['db_students'] # database

coll = db['students']   # collection

for doc in coll.find():
    print(doc)

# Detele documents whose age is 27
#coll.delete_many({'age': 27})

# Delete one document whose age more than 27
coll.delete_many({"age":{"$lt":25}})


print("After deleting one document")
for doc in coll.find():
    print(doc)