from pymongo import MongoClient

client = MongoClient('localhost', 27017)

# Create a database called "mydatabase":
mydb = client['mydb']
print("Database created!")

collection = mydb['users'] #collection
print("Collection created!")

users = [
    {
        "_id" : "101",
        "name" : "Steven",
        "age" : 25,
        "city" : "Jakarta"
    },
     {
        "_id" : "102",
        "name" : "Hendro",
        "age" : 26,
        "city" : "Bandung"
    },
      {
        "_id" : "103",
        "name" : "Budi",
        "age" : 24,
        "city" : "Tangerang"
    }
]  
res = collection.insert_many(users) #insert many documents
print(res.inserted_ids)
