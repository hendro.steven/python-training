from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client['db_students'] # database

coll = db['students']   # collection

data = [
    # {"_id" : "101", "name" : "Steven", "age" : 25, "city" : "Jakarta"},
    # {"_id" : "102", "name" : "Hendro", "age" : 26, "city" : "Bandung"},
    #{"_id" : "105", "name" : "Andre", "age" : 27, "city" : "Tangerang"},
    {"_id" : "106", "name" : "Joni", "age" : 27, "city" : "Tangerang"},
    {"_id" : "107", "name" : "John", "age" : 27, "city" : "Tangerang"},
    {"_id" : "108", "name" : "Paul", "age" : 30, "city" : "Bogor"}
    
]

res = coll.insert_many(data) #insert many documents

# print("First document of the collection")
# print(coll.find_one()) #find one document

# print("Document whose id is 103:")
# print(coll.find_one({"_id":"103"})) #find one document

# print("Document whose name is Budi:")
# print(coll.find_one({"name":"Budi"})) #find one document