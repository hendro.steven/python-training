from pymongo import MongoClient
import re

client = MongoClient('localhost', 27017)

db = client['db_students'] # database

coll = db['students']   # collection

for doc1 in coll.find():
    print(doc1) #find all documents
    
print("First documents whose age is more than 25:")
for doc2 in coll.find({"age":{"$gte":25}}):
    print(doc2) #find all documents whose age is more than 25
    
print("documents whose city is Tangerang:")
for doc3 in coll.find({"city":"Tangerang"}):
    print(doc3) #find all documents whose city is Tangerang
    

rgx = re.compile("^Hen", re.IGNORECASE) #compile a regular expression
print("documents whose name like Hen")
for doc4 in coll.find({"name": rgx}):
    print(doc4) #find all documents whose name like Hendro

    
    