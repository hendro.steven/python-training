from pymongo import MongoClient

client = MongoClient('localhost', 27017)

# Create a database called "mydatabase":
mydb = client['mydatabase']
print("Database created!")

collection = mydb['customers'] #collection
print("Collection created!")

customer = { "name": "Steven", "address": {"city":"Jakarta", "phone":"12343434","postal_code":"23434"} } #document
collection.insert_one(customer) #insert one document
print("Document inserted!")

print("List of databases:")
print(client.list_database_names())