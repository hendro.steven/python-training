from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client['db_students'] # database

coll = db['students']   # collection

for doc in coll.find():
    print(doc)

# Detele one document whose id is 108
coll.delete_one({'_id': "108"})

print("After deleting one document")
for doc in coll.find():
    print(doc)