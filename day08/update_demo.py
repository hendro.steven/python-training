from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client['db_students'] # database

coll = db['students']   # collection

for doc1 in coll.find():
    print(doc1)

# Update the document in the collection
coll.update_one({'_id': '101'}, {'$set': {'age': 30}})

print("After updating the document")

for doc2 in coll.find():
    print(doc2)