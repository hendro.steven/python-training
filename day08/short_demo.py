from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client['db_students'] # database

coll = db['students']   # collection

for doc in coll.find().sort("age", -1).limit(2):
    print(doc)