from pymongo import MongoClient
import re

client = MongoClient('localhost', 27017)

db = client['db_students'] # database

coll = db['students']   # collection

# for doc1 in coll.find():
#     print(doc1)
    

for doc2 in coll.find({"age":{"$gt": 26},"name": rgx}):
    print(doc2) #find all documents whose age is more than 26 and name like Hendro

rgx = re.compile("^Jo", re.IGNORECASE) 
coll.update_many({"age":{"$gt": 26},"name": rgx}, {'$set': {'city': 'Jakarta', 'age': 35}})

print("After updating the document")
for doc1 in coll.find():
    print(doc1)