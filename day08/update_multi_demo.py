from pymongo import MongoClient

client = MongoClient('localhost', 27017)

db = client['db_students'] # database

coll = db['students']   # collection

for doc1 in coll.find():
    print(doc1)
    
coll.update_many({}, {'$set': {'city': 'Tangerang'}})

print("After updating the document")

for doc2 in coll.find():
    print(doc2)