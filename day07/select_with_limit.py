from mysql.connector import connection

dict = {
    'user' : 'root',
    'password' : 'admin',
    'host' : 'localhost',
    'database' : 'db_python_training'
}
conn = connection.MySQLConnection(**dict)
cursor = conn.cursor() 

sql = "SELECT * FROM products LIMIT 10 OFFSET 0" # LIMIT 10 OFFSET 0 is the same as LIMIT 10 for mysql

cursor.execute(sql)

results = cursor.fetchall()

for result in results:
    print(result)

conn.close()