def menu():
    print("=== APLIKASI DATABASE PYTHON ===")
    print("1. Insert Data")
    print("2. Update Data")
    print("3. Delete Data")
    print("4. Select/Search Data")
    print("5. Exit")
    print("--------------------------------")
    option = input("Please choose [1-5] : ")
    return option

def insert_data():
    print('Insert Data')

def update_data():
    print('Update Data')

def delete_data():
    print('Delete Data')

def select_data():
    print('Select Data')
 
def app():
    option = 0
    while option != "5":
        option = menu()
        if option == "1":
            insert_data()
        elif option == "2":
            update_data()
        elif option == "3":
            delete_data()
        elif option == "4": 
            select_data()   
        
app()