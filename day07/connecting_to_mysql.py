import mysql.connector

conn = mysql.connector.connect(
    user = 'root', # set to your username
    password = 'admin', # set to your password
    host = 'localhost', # set to IP address of cloud server
    database = 'xxx'  # set to database name
)

print(conn)

conn.close()