from mysql.connector import connection

dict = {
    'user' : 'root',
    'password' : 'admin',
    'host' : 'localhost',
    'database' : 'db_python_training'
}

conn = connection.MySQLConnection(**dict)

cursor = conn.cursor() 

sql = "UPDATE products SET category ='TELEPON PINTAR' WHERE brand = 'Apple'"
cursor.execute(sql)
conn.commit()

print(cursor.rowcount, "record(s) affected")

conn.close()