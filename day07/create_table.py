from mysql.connector import connection

dict = {
    'user' : 'root',
    'password' : 'admin',
    'host' : 'localhost',
    'database' : 'db_python_training'
}

conn = connection.MySQLConnection(**dict)

cursor = conn.cursor() # create a cursor object to execute queries, etc.

# create a table
student_table = """CREATE TABLE IF NOT EXISTS products (
        NAME VARCHAR(200) NOT NULL,
        BRAND VARCHAR(200) NOT NULL,
        PRICE DOUBLE NOT NULL,
        CATEGORY VARCHAR(100) NOT NULL
    )"""

cursor.execute(student_table)

print('Table created successfully...')

conn.close()