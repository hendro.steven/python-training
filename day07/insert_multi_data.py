from mysql.connector import connection

dict = {
    'user' : 'root',
    'password' : 'admin',
    'host' : 'localhost',
    'database' : 'db_python_training'
}

conn = connection.MySQLConnection(**dict)

cursor = conn.cursor() # create a cursor object to execute queries, etc.

# insert data
sql = "INSERT INTO products (NAME, BRAND, PRICE, CATEGORY) VALUES (%s, %s, %s, %s)"
val = [
    ("Apple iPhone 12", "Apple", 200, "Smartphone"),
    ("Apple iPhone 13", "Apple", 300, "Smartphone"),
    ("Apple iPhone 14", "Apple", 400, "Smartphone"),
    ("Apple iPhone 16", "Apple", 600, "Smartphone"),
    ("Android Phone 1", "Android", 200, "Smartphone"),
    ("Smartwatch 1", "Samsung", 100, "Smartwatch"),
    ("SmartTV 1", "Samsung", 100, "SmartTV"),
]

cursor.executemany(sql, val)
conn.commit()

print('Data inserted successfully...')

conn.close()