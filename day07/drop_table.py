from mysql.connector import connection

dict = {
    'user' : 'root',
    'password' : 'admin',
    'host' : 'localhost',
    'database' : 'db_python_training'
}

conn = connection.MySQLConnection(**dict)

cursor = conn.cursor() 
query = "DROP TABLE IF EXISTS `products`"
cursor.execute(query)
conn.commit()

conn.close()