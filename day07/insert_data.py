from mysql.connector import connection

dict = {
    'user' : 'root',
    'password' : 'admin',
    'host' : 'localhost',
    'database' : 'db_python_training'
}

conn = connection.MySQLConnection(**dict)

cursor = conn.cursor() # create a cursor object to execute queries, etc.

# insert data
sql = "INSERT INTO products (NAME, BRAND, PRICE, CATEGORY) VALUES (%s, %s, %s, %s)"
val = ("Apple iPhone 15", "Apple", 699, "Smartphone")

# try:
#     cursor.execute(sql, val)
#     cursor.execute(sql, val)
# except:
#     print('Error inserting data...')
#     conn.rollback()
# else:
#     print('Data inserted successfully...')
#     conn.commit()

cursor.execute(sql, val)
conn.commit()


print('Data inserted successfully...')

conn.close()