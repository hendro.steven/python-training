from mysql.connector import connection

dict = {
    'user' : 'root',
    'password' : 'admin',
    'host' : 'localhost',
    'database' : 'db_python_training'
}

conn = connection.MySQLConnection(**dict)

cursor = conn.cursor() # create a cursor object to execute queries, etc.

sql = "SELECT * FROM products"
#sql = "SELECT NAME,BRAND FROM products"

cursor.execute(sql)

# fetch all rows from the last executed statement
results = cursor.fetchall()

#print(results)
for result in results:
    print(result)

conn.close()