from mysql.connector import connection

dict = {
    'user' : 'root',
    'password' : 'admin',
    'host' : 'localhost',
    'database' : 'xxx'
}

conn = connection.MySQLConnection(**dict)

# create a database
cursor = conn.cursor() # create a cursor object to execute queries, etc.

cursor.execute("CREATE DATABASE IF NOT EXISTS db_python_training")

print('Successfully created a database named db_python_training')

conn.close()