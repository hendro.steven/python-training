from django.urls import path
from . import views

urlpatterns = [
    path('',views.posts_list),
    path('add/',views.post_add),
    path('edit/<int:id>/',views.post_edit),
    path('delete/<int:id>/',views.post_delete),
]
