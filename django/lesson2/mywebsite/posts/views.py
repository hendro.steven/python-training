from django.shortcuts import render, redirect, get_object_or_404
from .models import Post
from .forms import PostForm

# Create your views here.

def posts_list(request):
    posts = Post.objects.all()
    print(posts)
    return render(request, 'posts/posts_list.html', {'posts': posts})

def post_add(request):
    form = PostForm(request.POST or None)
    
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('/posts/')
        else:
            return render(request, 'posts/post_add.html', {'post': form.data, 'form': form})
    
    return render(request, 'posts/post_add.html', {'post': form.data})

def post_edit(request, id):
    # post = Post.objects.get(id=id)
    post = get_object_or_404(Post, id=id)
   
    form = PostForm(request.POST or None, instance=post)
    
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect('/posts/')
        else:
            return render(request, 'posts/post_edit.html', {'post': form.data, 'form': form})

    return render(request, 'posts/post_edit.html', {'post': post})

def post_delete(request, id):
    post = Post.objects.get(id=id)
    post.delete()
    return redirect('/posts/')