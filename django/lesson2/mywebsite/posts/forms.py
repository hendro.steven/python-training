from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'body', 'slug']
        error_messages = {
            'title': {
                'required': "The title field is required"
            },
            'body': {
                'required': "The body field is required"
            },
            'slug': {
                'required': "The slug field is required"
            }
        }