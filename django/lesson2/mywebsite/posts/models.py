from django.db import models

# Create your models here.
# models is a class that inherits from models.Model to map the class to a database table

class Post(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    slug = models.SlugField()
    date_posted = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title