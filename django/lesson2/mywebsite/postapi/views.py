from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from posts.models import Post
from .serializer import PostSerializer


# Create your views here.

@api_view(['GET'])
def welcome(request):
    return Response({'message':'Welcome to the Post API'})

@api_view(['GET'])
def post_list(request):
    posts = Post.objects.all()
    serializer = PostSerializer(posts, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def post_add(request):
    serializer = PostSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    else:
        return Response(serializer.errors, status=400)
    
