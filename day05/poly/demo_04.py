class Bird:
    def __init__(self):
        print("Bird is ready")

    def fly(self):
        print("Bird is flying")
        
class Sparrow(Bird):
    def fly(self):
        print("Sparrow is flying")
        

class Ostrich(Bird):
    def fly(self):
        print("Ostrich can't fly")
        

def callFly(obj):
    obj.fly()


obj_spr = Sparrow()
obj_ost = Ostrich()

callFly(obj_ost)


