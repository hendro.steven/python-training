class Gun:
    def __init__(self, bullets=0):
        self.bullets = bullets
        
    def fire(self):
        pass
    
    def reload(self, bullets):
        print("Reloading...")
        self.bullets = bullets
        

class Revolver(Gun):
    def __init__(self, bullets=0):
        super().__init__(bullets)
        
    def fire(self):
        if self.bullets > 0:
            #while self.bullets > 0:
            print("Door!")
            self.bullets -= 1
        else:
            print("Please reload!")
            

class Rifle(Gun):
    def __init__(self, bullets=0):
        super().__init__(bullets)
        
    def fire(self):
        if self.bullets > 0:
            #while self.bullets > 0:
            print("Trrt..trrt..trrt..")
            self.bullets -= 3
        else:
            print("Please reload!")

class Soldier:
    def __init__(self, name, gun=None):
        self.gun = gun
        self.name = name
        print("Soldier {} is ready".format(self.name))
        
    def set_gun(self, gun):
        self.gun = gun
        
    
    def shot(self):
        if self.gun is None:
            print("I don't have a gun!")
            return None
        self.gun.fire()
        
    def reload(self, bullets):
        if self.gun is None:
            print("I don't have a gun!")
            return None
        self.gun.reload(bullets)
        

gun = Gun(5)
revolver = Revolver(5)
rifle = Rifle(6)

hendro = Soldier("Hendro", revolver)
#hendro.set_gun(rifle)
#hendro.reload(9)
hendro.shot()
hendro.shot()
hendro.shot()
hendro.shot()
hendro.shot()
hendro.shot()

# rambo = Soldier("Rambo")
# rambo.set_gun(revolver)
# rambo.reload(4)
# rambo.shot()

# rambo.set_gun(rifle)
# rambo.shot()
# rambo.shot()
# rambo.shot()
