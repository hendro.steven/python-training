with open('demo.txt', 'r') as file:
    data = file.read()

print(data)
line = data.split('\n')
new_str = 'How are you?'
line.insert(2, new_str)
print(line)

with open('demo.txt', 'w') as file:
    file.write('\n'.join(line))

with open('demo.txt', 'r') as file:
    data = file.read()
print(data)
