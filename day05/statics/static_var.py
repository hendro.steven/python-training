class ComputerScienceStudent:
    stream = 'cse'                  # Class Variable or Static Variable
    
    def __init__(self,name,roll):
        self.name = name            # Instance Variable
        self.roll = roll            # Instance Variable
        

a = ComputerScienceStudent('Hendro',1)
b = ComputerScienceStudent('Rahmat',2)

print(a.stream)                     # prints "cse"
print(b.stream)                     # prints "cse"

print(a.name)                       # prints "Hendro"
print(b.name)                       # prints "Rahmat"
print(a.roll)                       # prints "1"
print(b.roll)                       # prints "2"

print("------------------------")
print(a.stream)                     # prints "cse"
print(b.stream)                     # prints "cse"
print(ComputerScienceStudent.stream) # prints "cse"
print(id(a.stream))
print(id(b.stream))
print(id(ComputerScienceStudent.stream))


print("------------------------")

a.stream = 'ece'                    # Object Variable
print(a.stream)                     # prints "ece"
print(b.stream)                     # prints "cse"
print(ComputerScienceStudent.stream) # prints "cse"

print("------------------------")

print(id(a.stream))
print(id(b.stream))
print(id(ComputerScienceStudent.stream))