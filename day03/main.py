import mymodule
import myothermodule
import myfunctions.addFunctions as myFunc
from myfunctions.moreFunctions import sayHello
import myfunctions.variableModule as myVar
import platform

mymodule.greeting("Hendro Steven")
myothermodule.sayHello()
myothermodule.sayGoodbye()
myothermodule.saySomething("Hello, World!")

print(myFunc.sumOfTwoNumbers(1, 2))
print(myFunc.sumOfThreeNumbers(1, 2, 3))

sayHello()

print(myVar.person["name"])
print(myVar.person["age"])

data1 = platform.system()
data2 = platform.architecture()
data3 = platform.machine()
data4 = platform.node()
data5 = platform.processor()
print(data1)
print(data2)
print(data3)
print(data4)
print(data5)
