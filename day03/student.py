class Student:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        
    def __str__(self):
        return "Name: " + self.name + ", Age: " + str(self.age)
    
    def myOwnFunc(self):
        print("Hello, I am " + self.name + ", " + str(self.age) + " years old.")
    

s1 = Student('John', 21)
s1.myOwnFunc()
print(s1)

s2 = Student('Mary', 22)
s2.myOwnFunc()

del s1.age
s1.myOwnFunc()
